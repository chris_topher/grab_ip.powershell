<#
       .SYNOPSIS
       This script gets the IP address on the current machine.
       .DESCRIPTION
       This script obtains the IP by parsing the output of the ipconfig command for you.
       .PARAMETER ViaWireless
       Specifies whether or not the IP address should be obtained from the Wi-Fi or the Ethernet.
#>

param (
    [switch]$ViaWireless = $false
 )
 
$outputFileName = "./tmpOutput.out"

If (Test-Path $outputFileName){
	Remove-Item $outputFileName
}

ipconfig >> $outputFileName

$thispath = $PSScriptRoot
$reader = [System.IO.File]::OpenText([System.String]::Concat($thispath , $outputFileName))
$foundIPSection = $false

class IPInformation {
  [string] $IPAddress
  [string] $Via
}

function ParseIPAddress ($Line) {
  $splittedLine = $Line.Split(':')
  $ipAddressStringWithSpace = $splittedLine[1];
  $ipAddressAndSpace = $ipAddressStringWithSpace.Split(' ')
  $ipAddress = $ipAddressAndSpace[1];
  return $ipAddress;
}

function DetermineViaString(){
  $via = ""
  if ($ViaWireless) {$via = "Wi-Fi"} else {$via = "Ethernet"}
  return $via;
}

try {
    for() {
        $line = $reader.ReadLine()
        if ($line -eq $null) { break }
        elseif ($foundIPSection -eq $false){
          if (!$ViaWireless){
            if ($line.Contains("Ethernet adapter Ethernet")){
              $foundIPSection = $true
            }
          }
          elseif ($ViaWireless){
              if ($line.Contains("Wireless LAN adapter Wireless Network Connection:")){
            $foundIPSection = $true
            }
          }
        }
        elseif ($foundIPSection -eq $true){
          if ($line.Contains("IPv4 Address")){
            return new-object IPInformation -Property @{IPAddress = ParseIPAddress -Line $line; Via = DetermineViaString}
            break
          }
        }
    }
}
finally {
  $via = ""
  if ($ViaWireless) {$via = "Wi-Fi"} else {$via = "Ethernet"}
  $previousVerboseValue =  $VerbosePreference
  $VerbosePreference = "Continue"
    if (!$foundIPSection){
      $verbose = "Could not found the IP address via {0}. Maybe use -ViaWireless ?" -f $via
      Write-Verbose $verbose
      }
  $VerbosePreference = $previousVerboseValue;
    $reader.Close()
    If (Test-Path $outputFileName){
    Remove-Item $outputFileName
    }
}
