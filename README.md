## README ##

>A powershell script to grab the current machine's IP, just because powershell is the best shell-scripting language.
>
> ~Chris

# Get Started #
You can clone this repo to grab the script and start using it !

The help and usage can be seen using

```powershell
Get-Help ./Grab-Ip.ps1 -Detailed
```

Cordially,
Chris